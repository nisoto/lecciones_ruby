#encoding utf-8

arreglo = [6,8,3,1,6,4,0,9,2,8,7,7]

# Imprimir el array
print "Arreglo: "
puts arreglo.join(", ")

# Largo del array
puts "Largo: #{arreglo.size}"

# Añadiendo elementos al array
arreglo << -1
arreglo << 3
arreglo << 10
arreglo << 5
arreglo << 7

# Imprimiendo el array con sus nuevos elementos
print "Nuevo arreglo: "
puts arreglo.join(", ") 

puts "Nuevo largo: #{arreglo.size}"

# Primer y último elemento del array
puts "Primer elemento: #{arreglo.first}"
puts "Último elemento: #{arreglo.last}"

# Imprimir el arreglo con el iterador each
print "Arreglo con each: "
arreglo.each do |ar|
	print "#{ar} "
end
print "\n"

# Sumatoria de todos los elementos del array
suma = 0
arreglo.each do |ar|
	suma += ar
end
puts "Sumatoria: #{suma}"

# Mayor elemento del array
mayor = -9999
pos1 = -1
arreglo.each_with_index do |ar,posicion|
	if ar > mayor
		mayor = ar
		pos1 = posicion
	end
end
puts "Mayor elemento: #{mayor}, en posicion: #{pos1}"

# Menor elemento del array
menor = 9999
pos2 = -1
arreglo.each_with_index do |ar,posicion|
	if ar < menor
		menor = ar
		pos2 = posicion
	end
end
puts "Mayor elemento: #{menor}, en posicion: #{pos2}"

# Imprimir el array sin valores repetidos (uniq)
print "Arreglo sin repetidos: "
puts arreglo.uniq.join(", ")

# Array ordenado ascendentemente
print "Arreglo ordenado ASC: "
puts arreglo.sort.join(", ")

# Array ordenado descendentemente
print "Arreglo ordenado DESC: "
puts arreglo.sort.reverse.join(", ")

# Buscar un elemento del array (ingresado por teclado)
print "Ingrese elemento a buscar: "
b = gets.chomp.to_i
# arreglo.include?(b) retorna TRUE si se encuentra y FALSE si no se encuentra
if arreglo.index(b)
	puts "#{b} se encuentra en posicion #{arreglo.index(b)}"
else
	puts "#{b} NO se encuentra"
end

# Agregar elementos al array desde teclado
print "Ingrese un elemento al array (0 = salir): "
a = gets.chomp.to_i
while a != 0
	arreglo << a
	print "Ingrese otro elemento al array (0 = salir): "
	a = gets.chomp.to_i
end
print "Arreglo final: "
puts arreglo.join(", ")